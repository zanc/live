TOP = /w
LOCALCONF = ${TOP}/in

VERSION = 10.4.0
CODENAME = buster

LIVE_ARCHES = amd64
TYPES = nonfree
BUILDS = standard

MIRROR = http://0.0.0.0:9999/debian/
APT_MIRROR = http://0.0.0.0:9999/debian/

EXTRA_PKGS = 

BUILD_SOURCE = no

sinclude packages.mk

define _sanity_checks
	if [ "$$(id -u)" != "0" ]; then \
		echo "superuser access required"; \
		exit 1; \
	fi
endef

ifneq (${EXTRA_PKGS},)
define _extra_pkgs
	sed -i -e '/^lwr/,/^$$/s/^\s*-t live-task-.*/&\n    -e "${EXTRA_PKGS}" \\/' \
		${LOCALCONF}/run-30live-wrapper
endef
endif

ifneq (${BUILD_SOURCE},yes)
define _disable_source
	#sed -i -e '/^# Grab source URLs/,/^$$/ctouch sources\.list' ${LOCALCONF}/live-customise.sh
	sed -i -e '/^\s*# Now make the source tarballs/,/^.*Source tarballs started at/d' \
		-e '/^\s*cd \$${OUT}\/source\/tar && checksum_files/d' \
		${LOCALCONF}/run-30live-wrapper
endef
endif

build:
	@chroot=$$(schroot --list --all-chroots 2>&1 | head -1); \
	if [ "$${chroot#W:}" != "$$chroot" ]; then \
		echo "No chroots available, try running \`sbuild-createchroot' first" >&2; \
		exit 1; \
	fi; \
	user=$$(id -un); \
	if ! getent group sbuild 2>/dev/null | grep -q "[^:]*$$user[^:]*$$"; then \
		echo "User $$user is not a part of sbuild group, try running \`sbuild-adduser $$user' first" >&2; \
		exit 1; \
	fi; \
	if schroot --list --all-sessions | grep -q '^session:debian-live$$'; then \
		echo "Session debian-live already exists, try running \`schroot --end-session -c session:debian-live' first" >&2; \
		exit 1; \
	fi; \
	schroot --begin-session -c $$chroot --session-name debian-live >/dev/null || exit 1; \
	rm -rf /var/lib/sbuild/build/debian-live; \
	mkdir -p /var/lib/sbuild/build/debian-live; \
	for f in $$(git ls-tree --name-only HEAD); do \
		cp -r $$f /var/lib/sbuild/build/debian-live; \
	done; \
	cp -r dotfiles /var/lib/sbuild/build/debian-live; \
	schroot --run-session -c session:debian-live -q -p -d /build/debian-live -u root -- ${MAKE} --no-print-directory _sbuild; \
	retval="$$?"; \
	schroot --end-session -c session:debian-live; \
	if [ "$$retval" != "0" ]; then \
		exit $$retval; \
	fi; \
	mv /var/lib/sbuild/build/debian-live/out .; \
	rm -rf /var/lib/sbuild/build/debian-live

_sbuild:
	@$(call _sanity_checks)
	@apt-get -q -y -o APT::Install-Recommends=0 install live-wrapper dctrl-tools apt-utils
	@mkdir -p ${TOP} ${LOCALCONF} ${TOP}/out/log
	@chmod +x live-setup/available/common.sh \
		live-setup/available/run-30live-wrapper \
		local/live-customise.sh live-config/*
	@cd live-setup/available/ && \
		cp common.sh run-30live-wrapper \
		${LOCALCONF}
	@cp local/live-customise.sh ${LOCALCONF}
	@echo 'VERSION="${VERSION}"' >>${LOCALCONF}/CONF.sh
	@echo 'CODENAME="${CODENAME}"' >>${LOCALCONF}/CONF.sh
	@echo 'LIVE_ARCHES="${LIVE_ARCHES}"' >>${LOCALCONF}/CONF.sh
	@echo 'TYPES="${TYPES}"' >>${LOCALCONF}/CONF.sh
	@echo 'BUILDS="${BUILDS}"' >>${LOCALCONF}/CONF.sh
	@sed -i -e '/^MIRROR=/d' -e '/^APT_MIRROR=/d' \
		-e '/^select_firmware_packages () {/,/^}/s/Packages\.gz -O- | gzip /Packages\.xz -O- | xz /' \
		-e '/Updating the live-wrapper clone/,/^$$/d' \
		${LOCALCONF}/run-30live-wrapper
	@$(call _extra_pkgs)
	@$(call _disable_source)
	@mkdir -p ${LOCALCONF}/live-config
	@cp live-config/* ${LOCALCONF}/live-config
	@if [ -d dotfiles ]; then \
		mkdir -p ${LOCALCONF}/dotfiles; \
		find dotfiles -mindepth 1 -maxdepth 1 -exec cp -r {} ${LOCALCONF}/dotfiles \;; \
	fi
	@sed -i \
		-e "/'cmdline': 'boot=live components splash quiet'/{s/ splash / nosplash /;s/ quiet/ quiet username=live noautologin/}" \
		-e "/'cmdline': 'boot=live components locales=%s quiet splash'/{s/splash'/nosplash'/;s/splash'/splash username=live noautologin'/}" \
		-e "/'description': 'Debian Installer',.*/s//&\n                             'cmdline': 'vga=788',/" \
		/usr/lib/python2.7/dist-packages/lwr/bootloader.py
	@sed -i \
		-e '/from shutil import.*/s//&, copyfile/' \
		-e "/# Create ISO image.*/s@@&\n        copyfile('/w/in/live-config/1200-etc-configure', os\.path\.join(self\.cdroot\.path, 'etc-configure'))\n        os.chmod(os\.path\.join(self\.cdroot\.path, 'etc-configure'), 493)@" \
		-e "/self\.cdroot = CDRoot()/s/.*/&\n        os\.symlink('d-i', os\.path\.join(self\.cdroot\.path, 'install'))/" \
		/usr/lib/python2.7/dist-packages/lwr/run.py
	@MIRROR="${MIRROR}" APT_MIRROR="${APT_MIRROR}" \
		${LOCALCONF}/run-30live-wrapper
	@mv ${TOP}/out .
	@chown -R ${SCHROOT_UID}:${SCHROOT_GID} out

#clean:
#	@$(call _sanity_checks)
#	@rm -rf ${TOP}/*

clean:
	@rm -rf out

deps:
	@$(call _sanity_checks)
	@apt-get install sbuild

.PHONY: build clean deps _sbuild
