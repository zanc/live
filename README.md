This is the original script that is being used by Debian developers to build their live images starting from Buster.

This repository existed because I got too much trouble setting up [live-setup], like having to setup qemu vm, having debian-cd user, [pettersson], [casulana], virtual networks, etc that will possibly mess up my system.

Also even though this repository uses live-setup scripts, it contains my custom packages and configurations (as it was the original intention of this repository), so bear in mind it's not vanilla. And even though it's not vanilla, you can remove my configurations if you will :^)

[live-setup]: https://salsa.debian.org/images-team/live-setup.git
[pettersson]: https://db.debian.org/machines.cgi?host=pettersson
[casulana]: https://db.debian.org/machines.cgi?host=casulana

Usage
-----

To check out this repository:

    git clone --recursive https://gitlab.com/zanc/live.git

This repository was meant to be used in combination with `sbuild` with `approx` served in `127.0.0.1:9999`. If you don't want to use `approx` and prefer to use network mirror, simply edit the `Makefile`:

    MIRROR = http://deb.debian.org/debian/
    APT_MIRROR = http://deb.debian.org/debian/

Package source is disabled by default, to enable:

    BUILD_SOURCE = yes

To add extra packages to the resulting ISOs:

    EXTRA_PKGS = package1 package2 package3

Also edit other variables if needed like `LIVE_ARCHES`, `TYPES`, `BUILDS`, etc but keep in mind to never modify `TOP` and `LOCALCONF`.

To build, simply type:

    make build

But be sure all the required dependencies have been installed, beforehand:

    make deps

The built images will be created inside `out` directory.

Customization
-------------

To customize the live-wrapper, simply edit the `local/live-customise.sh` file.

You can add dot files if you wish. To do so, simply clone your dot files repository inside `dotfiles/` directory:

    git clone https://github.com/zanculmarktum/dotfiles.git dotfiles
